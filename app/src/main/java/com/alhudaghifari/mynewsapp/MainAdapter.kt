package com.alhudaghifari.mynewsapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.alhudaghifari.mynewsapp.dao.NewsDaoModel

class MainAdapter() :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private var datas = ArrayList<NewsDaoModel>()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle: TextView
        val tvAuthor: TextView
        val tvUrl: TextView

        init {
            tvTitle = view.findViewById(R.id.tv_title)
            tvAuthor = view.findViewById(R.id.tv_author)
            tvUrl = view.findViewById(R.id.tv_url)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_news, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = datas[position]
        holder.tvTitle.text = item.title ?: "-"
        holder.tvAuthor.text = item.author ?: "-"
        holder.tvUrl.text = item.url ?: "-"
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    fun setNewData(list: ArrayList<NewsDaoModel>) {
        datas = list
        notifyDataSetChanged()
    }

    fun addNewData(list: ArrayList<NewsDaoModel>) {
        datas.addAll(list)
        notifyDataSetChanged()
    }
}