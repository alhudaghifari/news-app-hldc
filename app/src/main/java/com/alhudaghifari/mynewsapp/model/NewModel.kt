package com.alhudaghifari.mynewsapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewModel(

	@field:SerializedName("hits")
	val hits: List<HitsItem>? = null,

	@field:SerializedName("renderingContent")
	val renderingContent: RenderingContent? = null,

	@field:SerializedName("exhaustiveTypo")
	val exhaustiveTypo: Boolean? = null,

	@field:SerializedName("hitsPerPage")
	val hitsPerPage: Int? = null,

	@field:SerializedName("processingTimeMS")
	val processingTimeMS: Int? = null,

	@field:SerializedName("query")
	val query: String? = null,

	@field:SerializedName("nbHits")
	val nbHits: Int? = null,

	@field:SerializedName("page")
	val page: Int? = null,

	@field:SerializedName("params")
	val params: String? = null,

	@field:SerializedName("nbPages")
	val nbPages: Int? = null,

	@field:SerializedName("exhaustiveNbHits")
	val exhaustiveNbHits: Boolean? = null
) : Parcelable

@Parcelize
data class HighlightResult(

	@field:SerializedName("story_text")
	val storyText: StoryText? = null,

	@field:SerializedName("author")
	val author: Author? = null,

	@field:SerializedName("title")
	val title: Title? = null,

	@field:SerializedName("url")
	val url: Url? = null
) : Parcelable

@Parcelize
data class Title(

	@field:SerializedName("matchLevel")
	val matchLevel: String? = null,

	@field:SerializedName("fullyHighlighted")
	val fullyHighlighted: Boolean? = null,

	@field:SerializedName("value")
	val value: String? = null,

	@field:SerializedName("matchedWords")
	val matchedWords: List<String?>? = null
) : Parcelable

@Parcelize
data class Url(

	@field:SerializedName("matchLevel")
	val matchLevel: String? = null,

	@field:SerializedName("fullyHighlighted")
	val fullyHighlighted: Boolean? = null,

	@field:SerializedName("value")
	val value: String? = null,

	@field:SerializedName("matchedWords")
	val matchedWords: List<String?>? = null
) : Parcelable

@Parcelize
data class RenderingContent(
	val any: String? = null
) : Parcelable

@Parcelize
data class StoryText(

	@field:SerializedName("matchLevel")
	val matchLevel: String? = null,

	@field:SerializedName("value")
	val value: String? = null,

) : Parcelable

@Parcelize
data class Author(

	@field:SerializedName("matchLevel")
	val matchLevel: String? = null,

	@field:SerializedName("value")
	val value: String? = null,


) : Parcelable

@Parcelize
data class HitsItem(

	@field:SerializedName("author")
	val author: String? = null,

	@field:SerializedName("story_id")
	val storyId: Int? = null,

	@field:SerializedName("_tags")
	val tags: List<String?>? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("created_at_i")
	val createdAtI: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("points")
	val points: Int? = null,

	@field:SerializedName("_highlightResult")
	val highlightResult: HighlightResult? = null,

	@field:SerializedName("num_comments")
	val numComments: Int? = null,

	@field:SerializedName("story_title")
	val storyTitle: String? = null,

	@field:SerializedName("story_url")
	val storyUrl: String? = null,

	@field:SerializedName("relevancy_score")
	val relevancyScore: Int? = null,

	@field:SerializedName("objectID")
	val objectID: String? = null
) : Parcelable
