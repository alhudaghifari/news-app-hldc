package com.alhudaghifari.mynewsapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.alhudaghifari.mynewsapp.dao.NewsDaoModel
import com.alhudaghifari.mynewsapp.model.HitsItem

class MainViewModel : ViewModel() {

    private var repo = NewsRepository()

    fun getNews(query: String) : LiveData<ArrayList<NewsDaoModel>> {
        return repo.getNews(query)
    }
}