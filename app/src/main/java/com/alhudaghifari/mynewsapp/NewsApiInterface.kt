package com.alhudaghifari.mynewsapp

import com.alhudaghifari.mynewsapp.model.NewModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface NewsApiInterface {

    @GET("api/v1/search")
    fun getNews(@Query("query") query: String = ""): Call<NewModel>

}