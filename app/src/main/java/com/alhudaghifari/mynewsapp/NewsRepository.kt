package com.alhudaghifari.mynewsapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.alhudaghifari.mynewsapp.dao.NewsDao
import com.alhudaghifari.mynewsapp.dao.NewsDaoModel
import com.alhudaghifari.mynewsapp.model.HitsItem
import com.alhudaghifari.mynewsapp.model.NewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRepository {
    fun getNews(query: String) : LiveData<ArrayList<NewsDaoModel>> {
        val data = MutableLiveData<ArrayList<NewsDaoModel>>()
        val service : NewsApiInterface = ApiService.create().create(NewsApiInterface::class.java)
        service.getNews(query).enqueue(object: Callback<NewModel> {
            override fun onResponse(call: Call<NewModel>, response: Response<NewModel>) {
                response.body()?.hits?.let {
                    val list = ArrayList<NewsDaoModel>()
                    var i = 1
                    for (item in it) {
                        val dataku = NewsDaoModel(
                            title = item.title,
                            author = item.author,
                            url = item.url,
                            id = i,
                        )
                        i++
                        list.add(dataku)
                    }
                    data.postValue(list)
                }
            }

            override fun onFailure(call: Call<NewModel>, t: Throwable) {

            }
        })

        return data
    }
}