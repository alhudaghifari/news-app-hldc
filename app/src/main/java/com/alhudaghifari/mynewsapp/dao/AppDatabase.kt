package com.alhudaghifari.mynewsapp.dao

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(NewsDaoModel::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): NewsDaoModel
}
