package com.alhudaghifari.mynewsapp.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NewsDao {
    @Query("SELECT * FROM newsdaomodel")
    fun getAll(): List<NewsDaoModel>

    @Insert
    fun insertAll(users: NewsDaoModel)

    @Delete
    fun delete(user: NewsDaoModel)
}